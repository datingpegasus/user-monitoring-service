/**
 * Main application file
 */
import * as express from 'express'
import * as http from 'http'
import appSetup from './src/index'
import config from './config/environment'

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// Setup server
const app: express.Application = express();
const server = http.createServer(app);
appSetup().then((result) => {
    // Start server
    server.listen(config.port, config.ip, () => {
        console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
    });
});

// Expose app

// tslint:disable-next-line:align
export default server
