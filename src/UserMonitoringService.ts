import IMessage from './models/message';
import {IUser, IUserStatusChange} from './models/user';
import {IRedisService} from './redis/RedisService';
import Loging from './Loging';
import {IKafkaProducer} from './KafkaProducer';

import * as kafka from 'kafkajs'
export enum userStatus {
    online = 'online',
    offline = 'offline',
    busy = 'busy'
}
export enum userKafkaSendEventNames {
    newMessage= 'NEW_MESSAGE',
    messageDelivered = 'MessageDelivered'
}
export interface IUserMonitoringService {
    userStatusChange(userStatusChange: IUserStatusChange): Promise<boolean | string>;
    userMessageSaved(message: IMessage): Promise<boolean>;
    deliveredMessageSaved(message: IMessage): Promise<boolean>;
}
export default class UserMonitoringService implements IUserMonitoringService {
    private redisService: IRedisService;
    private kafkaProducer: IKafkaProducer;
    constructor(redisService: IRedisService, kafkaProducer: IKafkaProducer) {
        this.redisService = redisService;
        this.kafkaProducer = kafkaProducer;
    }
    public async userStatusChange(userStatusChange: IUserStatusChange): Promise<boolean | string> {
        try {
            const userRedisStatus = await this.redisService.get(userStatusChange.user.id);

            if (userRedisStatus === null || userRedisStatus !== userStatusChange.status)  {
                return await this.redisService.set(userStatusChange.user.id, userStatusChange.status)
            }
            return true;
        } catch (e) {
            return e.toString();
        }

    }

    /**
     * If delivered message is saved it should send to sending user ack
     * @param message
     */
    public async deliveredMessageSaved(message: IMessage): Promise<boolean> {
        try {
            const userRedisStatus = await this.redisService.get(message.senderId);
            if (userRedisStatus !== null && userRedisStatus !== userStatus.offline) {
                return await this.kafkaProducer.dispatchNewMessageDelivered(message)
            } else {
                return true;
            }
        } catch (e) {
            Loging.log(e.toString())
            return false;
        }
    }

    /**
     * After message is saved it should send message to receiverId
     * @param message
     */
    public async userMessageSaved(message: IMessage): Promise<boolean> {
        try {
            const userRedisStatus = await this.redisService.get(message.receiverId);
            console.log(userRedisStatus)
            if (userRedisStatus !== null && userRedisStatus !== userStatus.offline) {
                return await this.kafkaProducer.dispatchNewMessage(message)
            }
            return true;
        } catch (e) {
            Loging.log(e.toString())
            return false;
        }
    }
}
