export default interface IMessage {
    id: string;
    conversationId: string;
    senderId: string;
    receiverId: string;
    message: string;
    type: number;
    attachmentUrl: string | null;
    delivered: boolean;
    sent: string | number;
    createdAt: string | number;
    updatedAt: string | number;
}
