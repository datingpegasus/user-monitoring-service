export interface IUser {
    id: string;
}
export interface IUserStatusChange {
    user: IUser;
    status: string;
}
