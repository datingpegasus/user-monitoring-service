export const messageData = {
    id: '123dsa',
    conversationId: '32131dasd',
    receiverId: '21313das',
    senderId: '321313',
    message: 'this is test message',
    type: 1,
    attachmentUrl: 'http://www.google.com',
    sent: '12/03/1991 12:43:00',
    createdAt: '12/03/1991 12:43:00',
    updatedAt: '12/03/1991 12:43:00',
    delivered: true
}
