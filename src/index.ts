import * as redis from 'redis'
import config from '../config/environment';
import KafkaProducer from './KafkaProducer';
import {kafka} from './modules/kafka';
import KafkaConsumer from './KafkaConsumer';
import UserMonitoringService from './UserMonitoringService';
import RedisService from './redis/RedisService';

export default async function appSetup() {
    try {
        const redisConnection = await redis.createClient(config.redisPort, config.redisUrl)
        const producer = kafka.producer();
        await producer.connect();
        const kafkaProducer = new KafkaProducer(producer);
        const consumer = kafka.consumer({groupId: 'monitoring-1'});
        await consumer.connect();
        const kafkaConsumer = new KafkaConsumer(consumer, new UserMonitoringService(new RedisService(redisConnection), kafkaProducer));
        await kafkaConsumer.subscribeToTopics();
        await consumer.run({
            autoCommit: false,
            eachMessage: async ({topic, partition, message}) => {
               switch (topic) {
                   case KafkaConsumer.topics.deliveredMessageSaved: {
                       await kafkaConsumer.deliveredMessageSaved(message, partition);
                       break
                   }
                   case KafkaConsumer.topics.userMessageSaved: {
                       await kafkaConsumer.userMessageSaved(message, partition);
                       break
                   }
                   case KafkaConsumer.topics.userStatusChange: {
                       await kafkaConsumer.userStatusChange(message, partition)
                       break;
                   }
                   default: {
                       console.log('missing method for kafka method:', topic);
                       break;
                   }
               }
           }
        });
    } catch (e) {
        console.log('here', e.toString())
    }
}
