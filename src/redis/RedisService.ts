import {RedisClient} from 'redis';

export interface IRedisService {
    set(key: string, value: string): Promise<boolean>;
    get(key: string): Promise<string>;
    exists(key: string): Promise<boolean>;
}
export default class RedisService implements IRedisService {
    private redisClient: RedisClient;

    constructor(redisClient: RedisClient) {
        this.redisClient = redisClient;
    }
    public async exists(key: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.redisClient.exists(key, (error, value) => {
                if (error) return reject(new Error(error.toString()))
                return resolve((value === 1))
            })
        });
    }

    public async get(key: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.redisClient.get(key, (error, value) => {
                if (error) return reject(new Error(error.toString()))
                return resolve(value)
            })
        })
    }

    public async set(key: string, value: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.redisClient.set(key, value, (error, res) => {
                if (error) return reject(new Error(error.toString()))
                return resolve(res === 'OK')
            })
        })
    }
}
