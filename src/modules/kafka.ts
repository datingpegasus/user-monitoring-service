import {Kafka} from 'kafkajs';

export const kafka = new Kafka({
    clientId: 'UserMonitoringService',
    brokers: ['192.168.0.15:9092']
})
