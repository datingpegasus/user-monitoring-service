import * as kafka from 'kafkajs'
import {IUserMonitoringService} from './UserMonitoringService';
import {validate} from 'validate-typescript';
import {messageSchema} from './validation/messageSchema';
import Loging from './Loging';
import {userStatusChangeSchema} from './validation/userSchema';
export interface IKafkaConsumer {
    userStatusChange(kafkaMessage: kafka.KafkaMessage, partition: number): Promise<void>
    userMessageSaved(kafkaMessage: kafka.KafkaMessage, partition: number): Promise<void>;
    deliveredMessageSaved(message: kafka.KafkaMessage, partition: number): Promise<void>;
    subscribeToTopics(): void;
}

export default class KafkaConsumer implements IKafkaConsumer {
    private kafkaConsumer: kafka.Consumer;
    private userMonitoringService: IUserMonitoringService;
    public static topics = {userMessageSaved: 'SavedMessage', deliveredMessageSaved: 'DeliveredMessageSaved', userStatusChange: 'UserStatusActivity'};
    constructor(kafkaConsumer: kafka.Consumer, userMonitoringService: IUserMonitoringService) {
        this.kafkaConsumer = kafkaConsumer;
        this.userMonitoringService = userMonitoringService;
    }

    public async deliveredMessageSaved(kafkaMessage: kafka.KafkaMessage, partition: number): Promise<void> {
        try {
            const kafkaParsedMessage = JSON.parse(kafkaMessage.value.toString());
            const saved = await this.userMonitoringService.deliveredMessageSaved(kafkaParsedMessage)
            if (saved)  {
                await this.kafkaConsumer.commitOffsets([{
                    topic: KafkaConsumer.topics.deliveredMessageSaved,
                    partition,
                    offset: kafkaMessage.offset
                }])
            }
        } catch (e) {
            Loging.log(e);
        }
    }

    public async userMessageSaved(kafkaMessage: kafka.KafkaMessage, partition: number): Promise<void> {
        try {
            const kafkaParsedMessage = JSON.parse(kafkaMessage.value.toString());
            validate(messageSchema, kafkaParsedMessage);
            const saved = await this.userMonitoringService.userMessageSaved(kafkaParsedMessage)
            if (saved)  {
                await this.kafkaConsumer.commitOffsets([{
                    topic: KafkaConsumer.topics.userMessageSaved,
                    partition,
                    offset: kafkaMessage.offset
                }])
            }
        } catch (e) {
            Loging.log(e);
        }
    }

    public async userStatusChange(kafkaMessage: kafka.KafkaMessage, partition: number): Promise<void> {
        try {
            const message = JSON.parse(kafkaMessage.value.toString());
            validate(userStatusChangeSchema, message);
            const saved = await this.userMonitoringService.userStatusChange(message);
            if (saved)  {
                await this.kafkaConsumer.commitOffsets([{
                    topic: KafkaConsumer.topics.userStatusChange,
                    partition,
                    offset: kafkaMessage.offset
                }])
            }
        } catch (e) {
            Loging.log(e.toString());
        }
    }

    public async subscribeToTopics(): Promise<void> {
        for (const [topicKey, topicValue] of Object.entries(KafkaConsumer.topics)) {
            await this.kafkaConsumer.subscribe({topic: topicValue})
        }
    }
}
