import IMessage from './models/message';
import * as kafka from 'kafkajs';

export interface IKafkaProducer {
    dispatchNewMessage(message: IMessage): Promise<boolean>
    dispatchNewMessageDelivered(message: IMessage): Promise<boolean>
}
export default class KafkaProducer implements IKafkaProducer {
    private producer: kafka.Producer;
    public static messageDelivered = 'MessageDelivered';
    public static newMessage = 'NewMessage';
    constructor(producer: kafka.Producer) {
        this.producer = producer;
    }
    public async dispatchNewMessageDelivered(message: IMessage): Promise<boolean> {
        const sent = await this.producer.send({
            topic: KafkaProducer.messageDelivered + '-' + message.senderId,
            messages: [
                { value: JSON.stringify(message) }
                ]
        })
        return (sent[0].errorCode === 0);
    }
    public async dispatchNewMessage(message: IMessage): Promise<boolean> {
        const sent = await this.producer.send({
            topic: KafkaProducer.newMessage + '-' + message.receiverId,
            messages: [
                { value: JSON.stringify(message) }
            ]
        })
        return (sent[0].errorCode === 0);
    }
}
