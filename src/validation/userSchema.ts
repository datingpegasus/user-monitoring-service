import {Options, Type} from 'validate-typescript'
import {IUser} from '../models/user';
import {userStatus} from '../UserMonitoringService';

export const userStatusChangeSchema = {
    user: {
        id: Type(String)
    },
    status: Options([userStatus.online, userStatus.offline, userStatus.busy])
}
