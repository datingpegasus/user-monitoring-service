import {Optional, Options, Type} from 'validate-typescript';

export const messageSchema = {
    id: Type(String),
    conversationId: Type(String),
    senderId: Type(String),
    receiverId: Type(String),
    message: Type(String),
    type: Options([0, 1, 2, 3]),
    attachmentUrl: Optional(Type(String)),
    delivered: Type(Boolean),
    sent: Type(String),
    createdAt: Type(String),
    updatedAt: Type(String)
}
