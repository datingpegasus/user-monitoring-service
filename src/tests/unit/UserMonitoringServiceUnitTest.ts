import * as sinon from 'sinon'
import {expect} from 'chai'
import RedisService from '../../redis/RedisService'
import {userStatusChange} from '../../models/userData'
import UserMonitoringService, {IUserMonitoringService, userKafkaSendEventNames, userStatus} from '../../UserMonitoringService'
import KafkaProducer, {IKafkaProducer} from '../../KafkaProducer';
import {kafka} from '../../modules/kafka';
import {Producer} from 'kafkajs';
import {messageData} from '../../models/messageData';
let kafkaProducer: sinon.SinonStubbedInstance<KafkaProducer>;
let redisClient: sinon.SinonStubbedInstance<RedisService>
let userMonitoringService: IUserMonitoringService;
describe('User Monitoring Service Unit Testing', () => {
    beforeEach(() => {
        kafkaProducer = sinon.createStubInstance(KafkaProducer);
        redisClient = sinon.createStubInstance(RedisService)
        userMonitoringService = new UserMonitoringService(redisClient, kafkaProducer);
    })
    describe('User Monitoring Service userStatusChange tests', () => {
        it('It should set user in status in redis and return true', async () => {
            redisClient.get.withArgs(userStatusChange.user.id).resolves(null);
            redisClient.set.withArgs(userStatusChange.user.id, 'online').resolves(true)
            const userDelivered = await userMonitoringService.userStatusChange(userStatusChange)
            expect(redisClient.set.calledOnce).to.be.true
            expect(redisClient.get.withArgs(userStatusChange.user.id).calledOnce);
            expect(userDelivered).to.be.true
        });
        it('It should return true if user with the same status exist in redis', async () => {
            redisClient.get.withArgs(userStatusChange.user.id).resolves('online');
            const userDelivered = await userMonitoringService.userStatusChange(userStatusChange)
            expect(redisClient.get.withArgs(userStatusChange.user.id).calledOnce).to.be.true;
            expect(redisClient.set.calledOnce).to.be.false
            expect(userDelivered).to.be.true;
        });
        it('It should return true if same user with the different status is added', async () => {
            redisClient.get.withArgs(userStatusChange.user.id).resolves('offline');
            redisClient.set.withArgs(userStatusChange.user.id, 'online').resolves(true)
            const userDelivered = await userMonitoringService.userStatusChange(userStatusChange);
            expect(redisClient.get.withArgs(userStatusChange.user.id).calledOnce).to.be.true;
            expect(redisClient.set.withArgs(userStatusChange.user.id, 'online').calledOnce).to.be.true;
            expect(userDelivered).to.be.true;
        });
        it('It should throw exception from redis service and resolves an error', async () => {
            redisClient.get.withArgs(userStatusChange.user.id).rejects('Error');
            const userDelivered = await userMonitoringService.userStatusChange(userStatusChange);
            expect(redisClient.get.withArgs(userStatusChange.user.id).calledOnce).to.be.true;
            expect(redisClient.set.calledOnce).to.be.false
            expect(userDelivered).to.be.eq('Error')
        });
    });
    describe('User monitoring service userDeliveredMessage',  () => {
        it('it should not emit if user is not found in redis', async () => {
            redisClient.get.withArgs(messageData.senderId).resolves(null);
            const userDelivered = await userMonitoringService.deliveredMessageSaved(messageData)
            expect(userDelivered).to.be.true;
            expect(kafkaProducer.dispatchNewMessageDelivered.withArgs(messageData).notCalled).to.be.true
        });
        it('it should emit if user is found in redis and status his is online', async () => {
            redisClient.get.withArgs(messageData.senderId).resolves(userStatus.online);
            kafkaProducer.dispatchNewMessageDelivered.withArgs(messageData).resolves(true)
            const userDelivered = await userMonitoringService.deliveredMessageSaved(messageData)
            expect(userDelivered).to.be.true;
            expect(kafkaProducer.dispatchNewMessageDelivered.withArgs(messageData).calledOnce).to.be.true
        });
        it('it should not emit if user is found in redis and his status  is offline', async () => {
            redisClient.get.withArgs(messageData.senderId).resolves(userStatus.offline);
            const userDelivered = await userMonitoringService.deliveredMessageSaved(messageData)
            expect(userDelivered).to.be.true;
            expect(kafkaProducer.dispatchNewMessageDelivered.withArgs(messageData).notCalled).to.be.true
        });
    });
    describe('User monitoring service userMessageSaved', () => {
        it('it should not emit if user is not found in redis', async () => {
            redisClient.get.withArgs(messageData.receiverId).resolves(null);
            const userDelivered = await userMonitoringService.userMessageSaved(messageData)
            expect(userDelivered).to.be.true;
            expect(kafkaProducer.dispatchNewMessage.withArgs(messageData).notCalled).to.be.true
        });
        it('it should emit if user is found in redis and status his is online', async () => {
            redisClient.get.withArgs(messageData.receiverId).resolves(userStatus.online);
            kafkaProducer.dispatchNewMessage.withArgs(messageData).resolves(true)
            const userDelivered = await userMonitoringService.userMessageSaved(messageData)
            expect(userDelivered).to.be.true;
            expect(kafkaProducer.dispatchNewMessage.withArgs(messageData).calledOnce).to.be.true
        });
        it('it should not emit if user is found in redis and his status  is offline', async () => {
            redisClient.get.withArgs(messageData.receiverId).resolves(userStatus.offline);
            const userDelivered = await userMonitoringService.userMessageSaved(messageData)
            expect(userDelivered).to.be.true;
            expect(kafkaProducer.dispatchNewMessage.withArgs(messageData).notCalled).to.be.true
        });
    })
});
