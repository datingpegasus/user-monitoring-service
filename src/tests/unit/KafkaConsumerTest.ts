import * as sinon from 'sinon'
import Loging from '../../Loging';
import KafkaConsumer, {IKafkaConsumer} from '../../KafkaConsumer';
import KafkaProducer from '../../KafkaProducer';
import {kafka} from '../../modules/kafka';
import UserMonitoringService, {IUserMonitoringService} from '../../UserMonitoringService';
import {messageData} from '../../models/messageData';
import {expect} from 'chai'
import {validate} from 'validate-typescript';
import {messageSchema} from '../../validation/messageSchema';
import {Consumer, Kafka} from 'kafkajs';
import {userStatusChange} from '../../models/userData';
const kafkaMessage = {
    key: new Buffer('1'),
    value: new Buffer(JSON.stringify(messageData)),
    timestamp: '123456',
    size: 1234,
    attributes: 123,
    offset: '1'
};
const userStatusKafkaMessage = {
    key: new Buffer('1'),
    value: new Buffer(JSON.stringify(userStatusChange)),
    timestamp: '123456',
    size: 1234,
    attributes: 123,
    offset: '1'
}
let kafkaInstance: sinon.SinonStubbedInstance<Consumer>;
let userMonitoringService: sinon.SinonStubbedInstance<IUserMonitoringService>;
let kafkaConsumer: IKafkaConsumer;
describe('Unit Test For Kafka Consumer', () => {
    describe('Unit tests for kafka consumer delivered message saved', () => {
        beforeEach(() => {
            kafkaInstance = sinon.stub(kafka.consumer({groupId: 'test'}));
            userMonitoringService = sinon.createStubInstance(UserMonitoringService);
            kafkaConsumer = new KafkaConsumer(kafkaInstance, userMonitoringService);
        });
        afterEach(() => {
            sinon.reset()
        })
        it('user should be online and message emited and offset committed ', async () => {
            userMonitoringService.deliveredMessageSaved.resolves(true)
            await kafkaConsumer.deliveredMessageSaved(kafkaMessage, 1)
            expect(userMonitoringService.deliveredMessageSaved.calledOnceWith(messageData)).to.be.true
            expect(kafkaInstance.commitOffsets.calledOnceWith([
                {
                    topic: KafkaConsumer.topics.deliveredMessageSaved,
                    partition: 1,
                    offset: '1'
                }
            ])).to.be.true
        });
        it('should not commit offset if user monitoring service returns false',  async () => {
            userMonitoringService.deliveredMessageSaved.resolves(false)
            await kafkaConsumer.deliveredMessageSaved(kafkaMessage, 1)
            expect(userMonitoringService.deliveredMessageSaved.calledOnceWith(messageData)).to.be.true
            expect(kafkaInstance.commitOffsets.calledOnceWith([
                {
                    topic: KafkaConsumer.topics.deliveredMessageSaved,
                    partition: 1,
                    offset: '1'
                }
            ])).to.be.false
        });
        it('should not call anything if json parse throw exception', async () => {
            const json = sinon.stub(JSON)
            json.parse.throwsException();
            userMonitoringService.deliveredMessageSaved.resolves(false)
            await kafkaConsumer.deliveredMessageSaved(kafkaMessage, 1)
            expect(userMonitoringService.deliveredMessageSaved.notCalled).to.be.true
            expect(userMonitoringService.deliveredMessageSaved.calledOnceWith(messageData)).to.be.false
            expect(kafkaInstance.commitOffsets.calledOnceWith([
                {
                    topic: KafkaConsumer.topics.deliveredMessageSaved,
                    partition: 1,
                    offset: '1'
                }
            ])).to.be.false
            json.parse.restore()
        })
    });
    describe('Testing Consume User Message Saved', () => {
        beforeEach(() => {
            kafkaInstance = sinon.stub(kafka.consumer({groupId: 'test'}));
            userMonitoringService = sinon.createStubInstance(UserMonitoringService);
            kafkaConsumer = new KafkaConsumer(kafkaInstance, userMonitoringService);
        });
        it('user should be online and new message emited and offset committed', async () => {
            userMonitoringService.userMessageSaved.resolves(true)
            await kafkaConsumer.userMessageSaved(kafkaMessage, 1)
            expect(userMonitoringService.userMessageSaved.calledOnceWith(messageData)).to.be.true
            expect(kafkaInstance.commitOffsets.calledOnceWith([
                {
                    topic: KafkaConsumer.topics.userMessageSaved,
                    partition: 1,
                    offset: '1'
                }
            ])).to.be.true
        });
        it('should not commit offset if user monitoring service returns false for new message', async () => {
            userMonitoringService.userMessageSaved.resolves(false)
            await kafkaConsumer.userMessageSaved(kafkaMessage, 1)
            expect(userMonitoringService.userMessageSaved.calledOnceWith(messageData)).to.be.true
            expect(kafkaInstance.commitOffsets.calledOnceWith([
                {
                    topic: KafkaConsumer.topics.userMessageSaved,
                    partition: 1,
                    offset: '1'
                }
            ])).to.be.false
        });
        /*it('should not call anything if json parse throw exception in new message', async () => {
            userMonitoringService.userMessageSaved.throwsException(false)
            await kafkaConsumer.userMessageSaved(kafkaMessage, 1)
            expect(userMonitoringService.userMessageSaved.calledOnceWith(messageData)).to.be.true
            expect(kafkaInstance.commitOffsets.calledOnceWith([
                {
                    topic: KafkaConsumer.topics.userMessageSaved,
                    partition: 1,
                    offset: '1'
                }
            ])).to.be.false
        })*/
    });
    describe('Testing Consumer User Status Change', () => {
        beforeEach(() => {
            kafkaInstance = sinon.stub(kafka.consumer({groupId: 'test'}));
            userMonitoringService = sinon.createStubInstance(UserMonitoringService);
            kafkaConsumer = new KafkaConsumer(kafkaInstance, userMonitoringService);
        });
        afterEach(() => {
            sinon.reset()
        })
        it('user should come online and status should be set in redis', async () => {
            userMonitoringService.userStatusChange.resolves(true);
            await kafkaConsumer.userStatusChange(userStatusKafkaMessage, 1);
            expect(userMonitoringService.userStatusChange.calledOnceWith(userStatusChange)).to.be.true;
            expect(kafkaInstance.commitOffsets.calledOnceWith([
                {
                    topic: KafkaConsumer.topics.userStatusChange,
                    partition: 1,
                    offset: '1'
                }
            ])).to.be.true
        });
        it('should not commit offsets if user status is not saved in redis database', async () => {
            userMonitoringService.userStatusChange.resolves(false);
            await kafkaConsumer.userStatusChange(userStatusKafkaMessage, 1);
            expect(userMonitoringService.userStatusChange.calledOnceWith(userStatusChange)).to.be.true;
            expect(kafkaInstance.commitOffsets.calledOnceWith([
                {
                    topic: KafkaConsumer.topics.userStatusChange,
                    partition: 1,
                    offset: '1'
                }
            ])).to.be.false
        });
        it('should not commit offsets if there is exception', async () => {
            userMonitoringService.userStatusChange.throwsException(false);
            await kafkaConsumer.userStatusChange(userStatusKafkaMessage, 1);
            expect(userMonitoringService.userStatusChange.calledOnceWith(userStatusChange)).to.be.true;
            expect(kafkaInstance.commitOffsets.calledOnceWith([
                {
                    topic: KafkaConsumer.topics.userStatusChange,
                    partition: 1,
                    offset: '1'
                }
            ])).to.be.false
        })
    })
});
