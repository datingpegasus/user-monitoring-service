import * as redis from 'redis'
import config from '../../../config/environment';
import {expect} from 'chai'
import * as sinon from 'sinon'
import RedisService, {IRedisService} from '../../redis/RedisService';
let redisClient: redis.RedisClient;
describe('Test Redis Service', () => {
   describe('Test Redis Service Get Method', () => {
       before(async () => {
           redisClient = await redis.createClient(config.redisPort, config.redisUrl)
           await redisClient.flushall();
       });
       it('should resolve existing value from redis', async () => {
           const set = await redisClient.set('1', '2');
           const redisService = new RedisService(redisClient);
           expect(await redisService.get('1')).to.be.eq('2')
           await redisClient.flushall()
       });
       it('should resolve false if value not exists', async () => {
           const redisService = new RedisService(redisClient)
           expect(await redisService.get('1')).to.be.null
       });
       it('should resolve error on rejection', async () => {
          const redisService = sinon.createStubInstance(RedisService);
          redisService.get.withArgs('1').rejects(new Error('error'));
          try {
              await redisService.get('1')
          } catch (e) {
              expect(e.toString()).to.be.eq('Error: error')
          }
       });
   })
   describe('Testing Redis Exists Method', () => {
       before(async () => {
           redisClient = await redis.createClient(config.redisPort, config.redisUrl)
           await redisClient.flushall();
       });
       it('should return true for existing value in redis', async () => {
           await redisClient.set('5', '2')
           const redisService = new RedisService(redisClient)
           expect(await redisService.exists('5')).to.be.true
           await redisClient.flushall()
       })
       it('should return false for non existing value in redis', async () => {
           const redisService = new RedisService(redisClient)
           expect(await redisService.exists('3')).to.be.false
        });
       it('should resolve error on exists rejection', async () => {
           const redisService = sinon.createStubInstance(RedisService);
           redisService.exists.withArgs('1').rejects(new Error('error'));
           try {
               await redisService.exists('1')
           } catch (e) {
               expect(e.toString()).to.be.eq('Error: error')
           }
       });
    });
   describe('Testing set method in redis service', () => {
       before(async () => {
           redisClient = await redis.createClient(config.redisPort, config.redisUrl)
           await redisClient.flushall();
       });
       it('should set value in redis and return true', async () => {
           const redisService = new RedisService(redisClient)
           await redisClient.flushall()
           expect(await redisService.set('6', '6')).to.be.true
       });
   });
})
