import * as sinon from 'sinon'
import {expect, assert} from 'chai'
import RedisService, {IRedisService} from '../../redis/RedisService'
import {userStatusChange} from '../../models/userData'
import UserMonitoringService, {IUserMonitoringService} from '../../UserMonitoringService'
import * as redis from 'redis';
import config from '../../../config/environment';
import KafkaProducer from '../../KafkaProducer';
import {kafka} from '../../modules/kafka';
let connection: redis.RedisClient;
let redisService: IRedisService;
let userMonitoringService: IUserMonitoringService;
describe('User Monitoring Service Integration Testing', () => {
    describe('User Monitoring Service userStatusChange tests', () => {
        before(async () => {
            connection = await redis.createClient(config.redisPort, config.redisUrl)
            redisService = new RedisService(connection);
            userMonitoringService = new UserMonitoringService(redisService, sinon.createStubInstance(KafkaProducer));
        });
        beforeEach(async () => {
            await connection.flushall();
        });
        it('It should set user in status in redis and return true', async () => {
            const userDelivered = await userMonitoringService.userStatusChange(userStatusChange);
            expect(userDelivered).to.be.true;
            expect(await redisService.get(userStatusChange.user.id)).to.eq(userStatusChange.status)
        });
        it('Should return true if user with the same status exist in redis', async () => {
            await redisService.set(userStatusChange.user.id, userStatusChange.status);
            const userDelivered = await userMonitoringService.userStatusChange(userStatusChange);
            expect(userDelivered).to.be.true;
        });
        it('Should return true if same user with the different status is added', async () => {
            await redisService.set(userStatusChange.user.id, userStatusChange.status);
            expect(await redisService.get(userStatusChange.user.id)).to.eq(userStatusChange.status);
            // must decouple the object immutability
            const differentStatus =  JSON.parse(JSON.stringify(userStatusChange));
            differentStatus.status = 'offline';
            assert.equal(differentStatus.status, 'offline');
            const userDelivered = await userMonitoringService.userStatusChange(differentStatus);
            expect(userDelivered).to.be.true;
            expect(await redisService.get(userStatusChange.user.id)).to.be.eq('offline')
        });
        it('It should throw exception from redis service and resolves an error', async () => {
            connection.end(true);
            try {
                const userDelivered = await userMonitoringService.userStatusChange(userStatusChange);
            } catch (e) {
                expect(e).should.throw
            }
        });
    });
});
